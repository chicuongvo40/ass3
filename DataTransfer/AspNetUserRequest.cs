﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransfer
{
    public class AspNetUserRequest
    {
        

        public AspNetUserRequest()
        {
        }

        public AspNetUserRequest(int id, string email, string userName, string firstName, string lastName, string passwordHash)
        {
            Id = id;
            Email = email;
            UserName = userName;
            FirstName = firstName;
            LastName = lastName;
            PasswordHash = passwordHash;
        }

        public int Id { get; set; }
        [Required]
        public string UserName { get; set; }
      
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
      
        [MinLength(5)]
        [MaxLength(255)]
        public string? PasswordHash { get; set; }
    }
}
