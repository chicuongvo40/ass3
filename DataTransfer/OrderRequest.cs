﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransfer
{
    public class OrderRequest
    {
        public OrderRequest()
        {
        }

        public OrderRequest(int orderID, DateTime orderDate, DateTime requiredDate, DateTime shippedDate, string freight, int? memberId)
        {
            OrderID = orderID;
            OrderDate = orderDate;
            RequiredDate = requiredDate;
            ShippedDate = shippedDate;
            Freight = freight;
            MemberId = memberId;
        }

        public int OrderID { get; set; }
        [Required]
        public DateTime OrderDate { get; set; }
        [Required]
        public DateTime RequiredDate { get; set; }
        public DateTime ShippedDate { get; set; }
        [Required]
        public string Freight { get; set; }
        [Required]
        public int? MemberId { get; set; }
    }
}
