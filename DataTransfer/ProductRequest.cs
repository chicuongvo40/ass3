﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransfer
{
    public class ProductRequest
    {
 
        public ProductRequest(int productId, string productName, string weight, int categoryId, string unitsInStock, decimal unitPrice)
        {
            ProductId = productId;
            ProductName = productName;
            Weight = weight;
            CategoryId = categoryId;
            UnitsInStock = unitsInStock;
            UnitPrice = unitPrice;
        }

        public int ProductId { get; set; }
        [Required]
        public string ProductName { get; set; }
        [Required]
        public string Weight { get; set; }
        [Required]
        public int CategoryId { get; set; }
        [Required]
        public string UnitsInStock { get; set; }
        [Required]
        public decimal UnitPrice { get; set; }
   
    }
}
