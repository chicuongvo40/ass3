﻿using BusinessObjects.Models;
using DataAccess;
using Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        public void DeleteCategory(Category category)
       => CategoryDAO.DeleteCategory(category);

        public List<Category> GetCategories()
       => CategoryDAO.GetCategories();

        public Category GetCategoryById(int id)
        => CategoryDAO.FindCategoryById(id);

        public void SaveCategory(Category category)
       => CategoryDAO.SaveCategory(category);

        public void UpdateCategory(Category category)
       => CategoryDAO.UpdateCategory(category);
    }
}
