﻿using BusinessObjects.Models;
using DataAccess;
using Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public class AspNetUserRepository : IAspNetUserRepository
    {
        public void DeleteUser(AspNetUser user)
       => UserDAO.DeleteUser(user);

        public AspNetUser GetUserByEmail(string email)
        => UserDAO.FindUserByEmail(email);

        public AspNetUser GetUserById(int id)
       => UserDAO.FindUserById(id);

        public List<AspNetUser> GetUsers()
       => UserDAO.GetUsers();

        public void SaveUser(AspNetUser user)
      => UserDAO.SaveUser(user);

        public List<AspNetUser> Search(string keyword)
       => UserDAO.Search(keyword);

        public void UpdateUser(AspNetUser user)
       => UserDAO.UpdateUser(user);
    }
}
