﻿using BusinessObjects.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Interface
{
    public interface IAspNetUserRepository
    {
        void SaveUser(AspNetUser user);
        AspNetUser GetUserById(int id);
        AspNetUser GetUserByEmail(string email);
        List<AspNetUser> GetUsers();
        List<AspNetUser> Search(string keyword);
        void UpdateUser(AspNetUser user);
        void DeleteUser(AspNetUser user);
    }
}
