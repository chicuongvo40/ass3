﻿using BusinessObjects.Models;
using DataTransfer;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Repositories;
using Repositories.Interface;

namespace IdentityAJaxClient.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IAspNetUserRepository repository = new AspNetUserRepository();
        [HttpGet]
        public ActionResult<IEnumerable<AspNetUser>> GetUsers() => repository.GetUsers();
        [HttpGet("{id}")]
        public ActionResult<AspNetUser> GetUserById(int id) => repository.GetUserById(id);
        [HttpGet("Search/{keyword}")]
        public ActionResult<IEnumerable<AspNetUser>> Search(string keyword) => repository.Search(keyword);
        [HttpGet("Email/{email}")]
        public ActionResult<AspNetUser> GetUserByEmail(string email) => repository.GetUserByEmail(email);
        [HttpPost]
        public IActionResult PostMember(AspNetUserRequest memberRequest)
        {
            var member = new AspNetUser
            {
                UserName = memberRequest.UserName,
                NormalizedUserName = "",
                Email = memberRequest.Email,
                NormalizedEmail = "",
                EmailConfirmed = "",
                PasswordHash = memberRequest.PasswordHash,
                SecurityStamp = "",
                ConcurrencyStamp = "",
                PhoneNumber = "",
                PhoneNumberConfirmed = "",
                TwoFactorEnabled = "",
                LockoutEnd ="",
                LockoutEnabled = "",
                AccessFailedCount = "",
                FirstName = memberRequest.FirstName,
                LastName = memberRequest.LastName,
            };
            repository.SaveUser(member);
            return NoContent();
        }
        [HttpDelete("{id}")]
        public IActionResult DeleteMember(int id)
        {
            var c = repository.GetUserById(id);
            if (c == null)
            {
                return NotFound();
            }
            repository.DeleteUser(c);
            return NoContent();
        }

        [HttpPut("{id}")]
        public IActionResult PutCustomer(int id, AspNetUserRequest memberRequest)
        {
            var cTmp = repository.GetUserById(id);
            if (cTmp == null)
            {
                return NotFound();
            }

            cTmp.UserName = memberRequest.UserName;
            cTmp.NormalizedUserName = "";
            cTmp.Email = memberRequest.Email;
            cTmp.NormalizedEmail = "";
            cTmp.EmailConfirmed = "";
            cTmp.PasswordHash = memberRequest.PasswordHash;
            cTmp.SecurityStamp = "";
            cTmp.ConcurrencyStamp = "";
            cTmp.PhoneNumber = "";
            cTmp.PhoneNumberConfirmed = "";
            cTmp.TwoFactorEnabled = "";
            cTmp.LockoutEnd = "";
            cTmp.LockoutEnabled = "";
            cTmp.AccessFailedCount = "";
            cTmp.FirstName = memberRequest.FirstName;
            cTmp.LastName = memberRequest.LastName;

            if (memberRequest.PasswordHash != null && cTmp.PasswordHash != memberRequest.PasswordHash)
            {
                cTmp.PasswordHash = memberRequest.PasswordHash;
            }

            repository.UpdateUser(cTmp);
            return NoContent();
        }
    }
}

