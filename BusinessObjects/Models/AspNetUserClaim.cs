﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class AspNetUserClaim
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public string ClaimType { get; set; } = null!;
        public string ClaimValue { get; set; } = null!;

        public virtual AspNetUser? User { get; set; }
    }
}
