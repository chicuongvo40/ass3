﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class AspNetRole
    {
        public AspNetRole()
        {
            AspNetRoleClaims = new HashSet<AspNetRoleClaim>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string NormalizedName { get; set; } = null!;
        public string ConcurrencyStamp { get; set; } = null!;

        public virtual ICollection<AspNetRoleClaim> AspNetRoleClaims { get; set; }
    }
}
