﻿using System;
using System.Collections.Generic;

namespace BusinessObjects.Models
{
    public partial class OrderDetail
    {
        public int? OrderId { get; set; }
        public int? ProductId { get; set; }
        public decimal UnitPrice { get; set; }
        public string Quantity { get; set; } = null!;
        public string Discount { get; set; } = null!;

        public virtual Order? Order { get; set; }
        public virtual Product? Product { get; set; }
    }
}
