﻿using BusinessObjects.Models;
using DataTransfer;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Net.Http.Headers;
using System.Numerics;
using System.Text;
using System.Text.Json;

namespace ProductManagementWebClients.Controllers
{
    public class ProductController : Controller
    {
        private readonly HttpClient client = null;
        private string ProductApiUrl = "";
        public ProductController()
        {
            client = new HttpClient();
            ProductApiUrl = "https://localhost:7091/api/Product";
        }
        public async Task<IActionResult> Index()
        {
            var accessToken = HttpContext.Session.GetString("token");
            accessToken = accessToken.Replace("\"", "");
            var requestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                Content = new StringContent(".....", Encoding.UTF8, "text/plain"),
                RequestUri = new Uri(ProductApiUrl)
            };

            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var response = await client.SendAsync(requestMessage);
            var apiString = await response.Content.ReadAsStringAsync();
            List<Product> listProducts = JsonConvert.DeserializeObject<List<Product>>(apiString);
            Debug.WriteLine(listProducts.Count);
            return View(listProducts);
        }
        [HttpGet]
        public async Task<IActionResult> Search(string SearchValue)
        {
            var accessToken = HttpContext.Session.GetString("token");
            accessToken = accessToken.Replace("\"", "");
            var requestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                Content = new StringContent(".....", Encoding.UTF8, "text/plain"),
                RequestUri = new Uri(ProductApiUrl + "/Search/" + SearchValue)
            };
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var response = await client.SendAsync(requestMessage);
            var apiString = await response.Content.ReadAsStringAsync();

            List<Product> listProducts = JsonConvert.DeserializeObject<List<Product>>(apiString);
            return View(listProducts);
        }
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(DataTransfer.ProductRequest product)
        {

            var json = JsonConvert.SerializeObject(product);
            var accessToken = HttpContext.Session.GetString("token");
            accessToken = accessToken.Replace("\"", "");
            var requestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                Content = new StringContent(json, Encoding.UTF8, "application/json"),
                RequestUri = new Uri(ProductApiUrl)
            };
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var response = await client.SendAsync(requestMessage);

            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Error creating product.");
            }
            return View(product);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            try
            {
                
                var accessToken = HttpContext.Session.GetString("token");
                accessToken = accessToken.Replace("\"", "");
                var requestMessage = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    Content = new StringContent(".....", Encoding.UTF8, "text/plain"),
                    RequestUri = new Uri(ProductApiUrl+"/"+id)
                };

                requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                var response = await client.SendAsync(requestMessage);
               
                if (response.IsSuccessStatusCode)
                {
                    var apiString = await response.Content.ReadAsStringAsync();
                    Product productRequest = JsonConvert.DeserializeObject<Product>(apiString);
                    if (productRequest == null)
                    {
                        return NotFound();
                    }

                    // Pass the productRequest to the view for editing
                    return View(productRequest);
                }
                else
                {
                    // Handle the case where the API request was not successful
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                // Handle exceptions (e.g., log the error) and return an error view
                ModelState.AddModelError(string.Empty, "Error loading product for editing.");
                return View();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Edit(string id, string name, string weight, string cateId, string unit, string price)
        {
            try
            {
                ProductRequest productRequest = new ProductRequest(int.Parse(id),name,weight,int.Parse(cateId),unit, decimal.Parse(price));   
                var json = JsonConvert.SerializeObject(productRequest);
                Debug.WriteLine(json);
                var accessToken = HttpContext.Session.GetString("token");
                accessToken = accessToken.Replace("\"", "");
                var requestMessage = new HttpRequestMessage
                {
                    Method = HttpMethod.Put,
                    Content = new StringContent(json, Encoding.UTF8, "application/json"),
                    RequestUri = new Uri(ProductApiUrl + "/" + productRequest.ProductId)
                };

                requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                var response = await client.SendAsync(requestMessage);
                    if (response.IsSuccessStatusCode)
                    {
                        // Redirect to the Index action upon successful update
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        // Handle the case where the API request to update the product was not successful
                        ModelState.AddModelError(string.Empty, "Error updating product.");
                    }
                

                // If ModelState is not valid or the API request fails, return to the edit view
                return RedirectToAction("Edit");
            }
            catch (Exception ex)
            {
                // Handle exceptions (e.g., log the error) and return to the edit view with an error message
                ModelState.AddModelError(string.Empty, "Error updating product.");
                return RedirectToAction("Edit");
            }
        }

        public async Task<IActionResult> Delete(int id)
        {
            var accessToken = HttpContext.Session.GetString("token");
            accessToken = accessToken.Replace("\"", "");
            var requestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Delete,
                Content = new StringContent(".....", Encoding.UTF8, "text/plain"),
                RequestUri = new Uri(ProductApiUrl + "/" + id)
            };

            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var response = await client.SendAsync(requestMessage);
            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return NotFound();
            }
        }
    }
}

