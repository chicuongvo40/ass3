﻿using BusinessObjects.Models;
using DataTransfer;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Net.Http.Headers;
using System.Text;

namespace ProductManagementWebClients.Controllers
{
    public class UserController : Controller
    {
        private readonly HttpClient client;
        private string MemberApiUrl = "";

        public UserController()
        {
            client = new HttpClient();
            var contentType = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Add(contentType);
            MemberApiUrl = "https://localhost:7091/api/User"; // Điều chỉnh URL API của bạn
        }
        public async Task<IActionResult> Index()
        {
            var accessToken = HttpContext.Session.GetString("token");
            accessToken = accessToken.Replace("\"", "");
            var requestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                Content = new StringContent(".....", Encoding.UTF8, "text/plain"),
                RequestUri = new Uri(MemberApiUrl)
            };

            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var response = await client.SendAsync(requestMessage);
            var apiString = await response.Content.ReadAsStringAsync();
            List<AspNetUser> listProducts = JsonConvert.DeserializeObject<List<AspNetUser>>(apiString);

            List<AspNetUserRequest> list = new List<AspNetUserRequest>();
            AspNetUserRequest obj = new AspNetUserRequest();
            foreach (var item in listProducts)
            {
                obj = new AspNetUserRequest(item.Id, item.Email, item.UserName, item.FirstName, item.LastName, item.PasswordHash);
                list.Add(obj);
            }
            return View(list);
        }
        //public IActionResult Index()
        //{
        //    return View();
        //}
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(DataTransfer.AspNetUserRequest _user)
        {

            var json = JsonConvert.SerializeObject(_user);
            var accessToken = HttpContext.Session.GetString("token");
            accessToken = accessToken.Replace("\"", "");
            var requestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                Content = new StringContent(json, Encoding.UTF8, "application/json"),
                RequestUri = new Uri(MemberApiUrl)
            };
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var response = await client.SendAsync(requestMessage);

            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Error creating product.");
            }
            return View(_user);
        }
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            try
            {

                var accessToken = HttpContext.Session.GetString("token");
                accessToken = accessToken.Replace("\"", "");
                var requestMessage = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    Content = new StringContent(".....", Encoding.UTF8, "text/plain"),
                    RequestUri = new Uri(MemberApiUrl + "/" + id)
                };

                requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                var response = await client.SendAsync(requestMessage);

                if (response.IsSuccessStatusCode)
                {
                    var apiString = await response.Content.ReadAsStringAsync();
                    AspNetUser userRequest = JsonConvert.DeserializeObject<AspNetUser>(apiString);
                    if (userRequest == null)
                    {
                        return NotFound();
                    }

                    // Pass the productRequest to the view for editing
                    return View(userRequest);
                }
                else
                {
                    // Handle the case where the API request was not successful
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                // Handle exceptions (e.g., log the error) and return an error view
                ModelState.AddModelError(string.Empty, "Error loading product for editing.");
                return View();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Edit(string id, string email, string userName, string firstName, string lastName, string passwordHash)
        {
            try
            {
                AspNetUserRequest userRequest = new AspNetUserRequest(int.Parse(id), email, userName, firstName, lastName, passwordHash);
                var json = JsonConvert.SerializeObject(userRequest);
                var accessToken = HttpContext.Session.GetString("token");
                accessToken = accessToken.Replace("\"", "");
                var requestMessage = new HttpRequestMessage
                {
                    Method = HttpMethod.Put,
                    Content = new StringContent(json, Encoding.UTF8, "application/json"),
                    RequestUri = new Uri(MemberApiUrl + "/" + userRequest.Id)
                };

                requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                var response = await client.SendAsync(requestMessage);
                if (response.IsSuccessStatusCode)
                {
                    // Redirect to the Index action upon successful update
                    return RedirectToAction("Index");
                }
                else
                {
                    // Handle the case where the API request to update the product was not successful
                    ModelState.AddModelError(string.Empty, "Error updating product.");
                }


                // If ModelState is not valid or the API request fails, return to the edit view
                return RedirectToAction("Edit");
            }
            catch (Exception ex)
            {
                // Handle exceptions (e.g., log the error) and return to the edit view with an error message
                ModelState.AddModelError(string.Empty, "Error updating product.");
                return RedirectToAction("Edit");
            }
        }
        public async Task<IActionResult> Delete(int id)
        {
            var accessToken = HttpContext.Session.GetString("token");
            accessToken = accessToken.Replace("\"", "");
            var requestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Delete,
                Content = new StringContent(".....", Encoding.UTF8, "text/plain"),
                RequestUri = new Uri(MemberApiUrl + "/" + id)
            };

            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var response = await client.SendAsync(requestMessage);
            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return NotFound();
            }
        }
    }
}
