﻿using BusinessObjects.Models;
using Microsoft.AspNetCore.Mvc;
using ProductManagementWebClients.Models;
using System.Diagnostics;
using System.Diagnostics.Metrics;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace ProductManagementWebClients.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly HttpClient client;
        private string MemberApiUrl = "";
        public HomeController()
        {
            client = new HttpClient();
            var contentType = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Add(contentType);
            MemberApiUrl = "https://localhost:7091/login"; // Điều chỉnh URL API của bạn
        }
        [HttpPost]
        public async Task<IActionResult> Login(string email, string pass)
        {
            Debug.WriteLine(email);
            Debug.WriteLine(pass);
            LoginModel user = new LoginModel(email, pass);
            var json = JsonSerializer.Serialize(user);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PostAsync(MemberApiUrl, content);

            if (response.IsSuccessStatusCode)
            {
                string token = await response.Content.ReadAsStringAsync();
                HttpContext.Session.SetString("token", token);
                Debug.WriteLine(response.StatusCode);
                Debug.WriteLine(token);
                return RedirectToAction("Index", "Product");
            }
            else
            {
                Debug.WriteLine("Login Faild");
                return RedirectToAction("Index");
            }
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }



    }
}