﻿using BusinessObjects.Models;
using DataTransfer;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Net.Http.Headers;
using System.Text;

namespace ProductManagementWebClients.Controllers
{
    public class OrderController : Controller
    {
        private readonly HttpClient client = null;
        private string OrderApiUrl = "";
        public OrderController()
        {
            client = new HttpClient();
            OrderApiUrl = "https://localhost:7091/api/Order";
        }
        public async Task<IActionResult> Index()
        {
            var accessToken = HttpContext.Session.GetString("token");
            accessToken = accessToken.Replace("\"", "");
            var requestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                Content = new StringContent(".....", Encoding.UTF8, "text/plain"),
                RequestUri = new Uri(OrderApiUrl)
            };

            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var response = await client.SendAsync(requestMessage);
            var apiString = await response.Content.ReadAsStringAsync();
            List<Order> listOrder = JsonConvert.DeserializeObject<List<Order>>(apiString);
            Debug.WriteLine(listOrder.Count);
            return View(listOrder);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(DataTransfer.OrderRequest order)
        {

            var json = JsonConvert.SerializeObject(order);
            var accessToken = HttpContext.Session.GetString("token");
            accessToken = accessToken.Replace("\"", "");
            var requestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                Content = new StringContent(json, Encoding.UTF8, "application/json"),
                RequestUri = new Uri(OrderApiUrl)
            };
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var response = await client.SendAsync(requestMessage);

            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Error creating product.");
            }
            return View(order);
        }
        public async Task<IActionResult> Delete(int id)
        {
            var accessToken = HttpContext.Session.GetString("token");
            accessToken = accessToken.Replace("\"", "");
            var requestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Delete,
                Content = new StringContent(".....", Encoding.UTF8, "text/plain"),
                RequestUri = new Uri(OrderApiUrl + "/" + id)
            };

            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var response = await client.SendAsync(requestMessage);
            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return NotFound();
            }
        }
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            try
            {

                var accessToken = HttpContext.Session.GetString("token");
                accessToken = accessToken.Replace("\"", "");
                var requestMessage = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    Content = new StringContent(".....", Encoding.UTF8, "text/plain"),
                    RequestUri = new Uri(OrderApiUrl + "/" + id)
                };

                requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                var response = await client.SendAsync(requestMessage);

                if (response.IsSuccessStatusCode)
                {
                    var apiString = await response.Content.ReadAsStringAsync();
                    Order orderRequest = JsonConvert.DeserializeObject<Order>(apiString);
                    if (orderRequest == null)
                    {
                        return NotFound();
                    }

                    // Pass the productRequest to the view for editing
                    return View(orderRequest);
                }
                else
                {
                    // Handle the case where the API request was not successful
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                // Handle exceptions (e.g., log the error) and return an error view
                ModelState.AddModelError(string.Empty, "Error loading product for editing.");
                return View();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Edit(string orderID, DateTime orderDate, DateTime requiredDate, DateTime shippedDate, string freight, int? memberId)
        {
            try
            {
                OrderRequest orderRequest = new OrderRequest(int.Parse(orderID), orderDate, requiredDate, shippedDate, freight, memberId);
                var json = JsonConvert.SerializeObject(orderRequest);
                var accessToken = HttpContext.Session.GetString("token");
                accessToken = accessToken.Replace("\"", "");
                var requestMessage = new HttpRequestMessage
                {
                    Method = HttpMethod.Put,
                    Content = new StringContent(json, Encoding.UTF8, "application/json"),
                    RequestUri = new Uri(OrderApiUrl + "/" + orderRequest.OrderID)
                };

                requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                var response = await client.SendAsync(requestMessage);
                if (response.IsSuccessStatusCode)
                {
                    // Redirect to the Index action upon successful update
                    return RedirectToAction("Index");
                }
                else
                {
                    // Handle the case where the API request to update the product was not successful
                    ModelState.AddModelError(string.Empty, "Error updating product.");
                }


                // If ModelState is not valid or the API request fails, return to the edit view
                return RedirectToAction("Edit");
            }
            catch (Exception ex)
            {
                // Handle exceptions (e.g., log the error) and return to the edit view with an error message
                ModelState.AddModelError(string.Empty, "Error updating product.");
                return RedirectToAction("Edit");
            }
        }
    }
}

